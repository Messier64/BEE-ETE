import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react-swc';

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    proxy: {
      '/api': {
        target: 'https://serene-scribbles.onrender.com/',
        secure: false,
      },
    },
  },
  plugins: [react()],
});
